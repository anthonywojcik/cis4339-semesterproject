require 'csv'
namespace :import do
  task :create_cust => :environment do
    csv_text = File.read('db/cust_data.csv')
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      Customer.create!(row.to_hash)
    end
  end
end 