# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
build_table = ->
  total =  $("#sale_total").val()
  interest_rate = $("#sale_interest_rate").val()
  num_years = $("#sale_num_years").val()
  $.get "/sales/due",{total: total,interest_rate: interest_rate, num_years: num_years}

$ ->
  $('#sale_total,#sale_interest_rate,#sale_num_years').bind 'keyup mouseup mousewheel', ->
    build_table()


price = 0
pricefield= ->
  selection = $('#sale_vehicle').val()
  $.getJSON '/inventories/' + selection + '/price_with_tax', {},(json, response) ->
    $('#sale_total').val(json['price_with_tax'])
    price = parseFloat($('#sale_total').val()).toFixed(2)
$ ->
  $('#sale_vehicle').change -> pricefield()



