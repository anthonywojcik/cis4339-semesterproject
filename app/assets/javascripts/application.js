// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require_tree .

$("sale_vehicle").change(function(){  //calls this function when the selected value changes
    $.get("/sale/"+$(this).val()+"/get_json",function(data, status, xhr){
        data = eval(data);  //turn the response text into a javascript object
        $("sale_total").val(data.date_created);  //sets the value of the fields to the data returned
    });
});

