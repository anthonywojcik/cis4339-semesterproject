$("#sale_vehicle").change(function(){  //calls this function when the selected value changes
    $.get("/sale/"+$(this).val()+"/get_json",function(data, status, xhr){
        data = eval(data);  //turn the response text into a javascript object
        $("#sale_total").val(data.date_created);  //sets the value of the fields to the data returned
    });
});