class SalesController < ApplicationController
  before_action :set_sale, only: [:show, :edit, :update, :destroy]

  # GET /sales
  # GET /sales.json
  def get_json
    sale = Sale.find(params[:sale_id])
    render :json => sale.to_json
  end

  def index
    @sales = Sale.all
    @sales = Sale.where(nil)
    @sales = @sales.status(params[:status]) if params[:status].present?


    @msrp = Inventory.msrp(params[:msrp]) if params[:msrp].present?
    @markup = Inventory.msrp(params[:msrp]) if params[:msrp].present?
    @tax = Sale.markup(params[:markup]) if params[:markup].present?
  end

  # GET /sales/1
  # GET /sales/1.json
  def show
    #@loan_array = @sales.calculate_loan_table
    #@sale = Sale.find(params[:id])

    #render :text => sale.vehicle.price_with_tax
  end

  # GET /sales/new
  def new
    @sale = Sale.new

  end

  # GET /sales/1/edit
  def edit
  end

  # POST /sales
  # POST /sales.json
  def create
    @sale = Sale.new(sale_params)
    respond_to do |format|
      if @sale.save
        format.html { redirect_to @sale, notice: 'Sale was successfully created.' }
        format.json { render :show, status: :created, location: @sale }
      else
        format.html { render :new }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales/1
  # PATCH/PUT /sales/1.json
  def update
    respond_to do |format|
      if @sale.update(sale_params)
        format.html { redirect_to @sale, notice: 'Sale was successfully updated.' }
        format.json { render :show, status: :ok, location: @sale }
      else
        format.html { render :edit }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales/1
  # DELETE /sales/1.json
  def destroy
    @sale.destroy
    respond_to do |format|
      format.html { redirect_to sales_url, notice: 'Sale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def due
    total = params[:total]
    interest_rate = params[:interest_rate]
    num_years = params[:num_years]
    @rate_array = Sale.calculate_loan_table(total.to_f, interest_rate.to_f, num_years.to_f)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sale
      #@sale = Sale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sale_params
      params.require(:sale).permit(:vehicle, :total, :customer_id, :employee_id, :status)
    end
end

