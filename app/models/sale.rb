class Sale < ActiveRecord::Base
  belongs_to :customer
  belongs_to :employee
  belongs_to :inventory

  scope :status, -> (status) { where status: status }


  def get_json
    json = "{"
    json += "id:'#{Inventory.msrp}'"
    json += "}"
  end

  def self.calculate_loan_table(total, interest_rate, num_years)
    Rails.logger.debug "Inputs: #{total} #{interest_rate} #{num_years}"
    due = []
    (1..(num_years * 12)).each do
      due << (total * (1 + (interest_rate.to_f / 100) * num_years)) / (num_years*12)
    end
    return due
  end

  def interest(total, interest_rate)
    Rails.logger.debug "Inputs: #{total} #{interest_rate}"
    @interest = total * (interest_rate.to_f / 100)
  end

  def vehicle
    @vehicle
  end

  def interest_rate
    @interest_rate
  end

  def total
    @total
  end

  def num_years
    @num_years
  end

  def calculate_loan_table
    return self.class.calculate_loan_table(self.total,self.interest_rate,self.num_years)
  end

  def sum_revenue
    @revenue = Sale.where( :status => ["Sold"]).sum(:total)
  end
end
