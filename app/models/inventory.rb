class Inventory < ActiveRecord::Base
  has_many :sales

  def self.search(search)
    if search
      self.where("model like ? OR color like ? OR VIN like ?", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      self.all
    end
  end

  def self.color
    @color
  end

  def self.model
    @model
  end

  def self.vin
    @vin
  end

  def vehicle
    @vehicle = [color, model, vin].join(' ')
  end

  def sale_price
    @sale_price = msrp * 1.1
  end

  def price_with_tax
    @price_with_tax = sale_price * 1.043
  end
end
