json.array!(@sales) do |sale|
  json.extract! sale, :id, :vehicle, :total, :customer_id, :employee_id, :status
  json.url sale_url(sale, format: :json)
end
