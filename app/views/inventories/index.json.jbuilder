json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :model, :color, :vin, :msrp
  json.url inventory_url(inventory, format: :json)
end
