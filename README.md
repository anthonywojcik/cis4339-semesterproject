## READ ME ##

### What is this repository for? ###

Anthony Wojcik's CIS 4339 semester project resembling a car dealership management system web application.

### How do I get set up? ###

Run "db:seed" to populate Inventory and Employee tables.
Run "rake import:create_cust" to populate the Customer table