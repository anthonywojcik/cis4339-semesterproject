class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :sales_id
      t.string :model
      t.string :color
      t.string :vin
      t.decimal :msrp
      t.string :vehicle
      t.string :vehicle

      t.timestamps null: false
    end
  end
end
