class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :inventory_id
      t.string :vehicle
      t.double :total, :default => 0
      t.integer :customer_id
      t.integer :employee_id
      t.string :status
      t.double :interest_rate
      t.integer :num_years

      t.timestamps null: false
    end
  end
end
