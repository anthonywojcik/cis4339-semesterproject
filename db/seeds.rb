# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#(1..100).each do |t|
  Inventory.delete_all
  Employee.delete_all

  Inventory.create(:model => "Mercedes", :color => "Red", :vin => "R9S8JFIS4487DHJFG", :msrp => 56500)
  Inventory.create(:model => "Cadillac", :color => "Black", :vin => "1FAFP251X5G462234", :msrp => 42000)
  Inventory.create(:model => "Lamborghini", :color => "White", :vin => "WAUSFAFL0BA484325", :msrp => 190000)
  Inventory.create(:model => "Audi", :color => "Navy", :vin => "1GTFH15T861646224", :msrp => 34250)
  Inventory.create(:model => "Ferrari", :color => "Gray", :vin => "4T1BK46K68U440453", :msrp => 210000)
  Inventory.create(:model => "Tesla", :color => "Black", :vin => "1FTSW30L5XE475284", :msrp => 75000)
  Inventory.create(:model => "Corvette", :color => "Yellow", :vin => "1D7RE2GK2BS433960", :msrp => 82300)
  Inventory.create(:model => "Rolls-Royce", :color => "Silver", :vin => "JTMYF4DV6A5740641", :msrp => 260000)
  Inventory.create(:model => "Porsche", :color => "White", :vin => "1G1ZE5EB5AF216806", :msrp => 235100)
  Inventory.create(:model => "Acura", :color => "Navy", :vin => "1D7HE48NX8S795527", :msrp => 38600)
  Inventory.create(:model => "BMW", :color => "Black", :vin => "4M2CU97128K639799", :msrp => 88400)
  Inventory.create(:model => "Lexus", :color => "Beige", :vin => "1MEHM59S75A240705", :msrp => 71250)
  Inventory.create(:model => "Infiniti", :color => "Green", :vin => "JF1SF63592G725453", :msrp => 34600)
  Inventory.create(:model => "Ferrari", :color => "Red", :vin => "3GKEC16T04G541981", :msrp => 234250)
  Inventory.create(:model => "Porsche", :color => "Gray", :vin => "WVGCV7AX4BW345725", :msrp => 255110)
  Inventory.create(:model => "Lamborghini", :color => "Red", :vin => "1FTWW32S02E041370", :msrp => 215520)
  Inventory.create(:model => "BMW", :color => "White", :vin => "JTEHT05J162749250", :msrp => 112300)
  Inventory.create(:model => "Audi", :color => "White", :vin => "1G8AM14FX3Z277116", :msrp => 62150)
  Inventory.create(:model => "BMW", :color => "Gray", :vin => "1J4GW48J94C124008", :msrp => 32000)
  Inventory.create(:model => "Mercedes", :color => "Navy", :vin => "3GKGK26U43G399000", :msrp => 76500)
  Inventory.create(:model => "Lexus", :color => "Silver", :vin => "1YVHZ8CB2B5679502", :msrp => 96300)

  Employee.create(:name => "Jessica	Mcguire", :position => "Salesperson")
  Employee.create(:name => "Alberto	Pena", :position => "Salesperson")
  Employee.create(:name => "Art Vandelay", :position => "Salesperson")
  Employee.create(:name => "Myron	Roberson", :position => "Salesperson")
  Employee.create(:name => "Blake	Lindsey", :position => "Salesperson")
  Employee.create(:name => "Max	Nunez", :position => "Salesperson")
  Employee.create(:name => "Clifford	Luna", :position => "Salesperson")
  Employee.create(:name => "Clinton	Reeves", :position => "Salesperson")
  Employee.create(:name => "Allison	Fisher", :position => "Salesperson")
  Employee.create(:name => "Cedric	Marsh", :position => "Salesperson")
